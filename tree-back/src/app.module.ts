import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PersonModule } from './person/person.module';
import { DataBaseModule } from './database/database.module';
import { LocationModule } from './location/location.module';
import { EventModule } from './event/event.module';
import { RelationModule } from './relation/relation.module';

@Module({
  imports: [PersonModule, DataBaseModule, LocationModule, EventModule, RelationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
