import * as mongoose from 'mongoose'
import { DATABASE_TOKEN } from '../constants';

export const DatabaseProvider = [{
  provide: DATABASE_TOKEN,
  useFactory: (): Promise<typeof mongoose> =>
    mongoose.connect('mongodb://localhost/treedb', {useNewUrlParser: true }),
}]
