import ObjectId from 'bson'
import Model from 'mongoose'

export class DatabaseService<D,T> {
    constructor(private readonly model:Model<T>){
    }

    async insert(value : D): Promise<T>{
        const insertedValue = new this.model(value)
        return insertedValue.save();
    }

    async getAll(): Promise<T[]>{
        return this.model.find();
    }

    async getById(id : ObjectId): Promise<T>{
        return this.model.findById(id);
    }

    async update(id : ObjectId, value : D): Promise<T>{
        return this.model.findByIdAndUpdate(id, value);
    }

    async delete(id : ObjectId): Promise<T>{
        return this.model.findByIdAndDelete(id);
    }
}