import * as mongoose from 'mongoose';
const ObjectId = mongoose.Schema.Types.ObjectId;

export const Relation = {
    type : {
        type : String,
        required : true
    },
    person : {
        type : ObjectId,
        required : true
    }
}

export const RelationSchema = new mongoose.Schema(Relation);