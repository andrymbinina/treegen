import  { Document, ObjectID } from 'mongoose'

import { Person } from '../../person/interfaces/person.interface';

export class Relation extends Document{
    readonly type : String
    readonly person : ObjectID | Person
}