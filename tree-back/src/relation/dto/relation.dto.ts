import { PersonDto } from "../../person/dto/person.dto"
import ObjectID from "mongoose"

export interface RelationDto {
    readonly _id : ObjectID [];
    readonly type : String
    readonly person : ObjectID | PersonDto
}