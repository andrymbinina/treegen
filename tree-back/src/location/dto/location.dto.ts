import ObjectID from 'mongoose'

export class LocationDto {
    readonly _id : ObjectID [];
    readonly name : String
    readonly country : String
}