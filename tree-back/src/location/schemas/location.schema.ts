import * as mongoose from 'mongoose'

export const Location = {
    name : {
        type : String,
        required : true
    },
    country : {
        type : String
    }
}

export const LocationSchema = new mongoose.Schema(Location);