import { Document } from 'mongoose'

export class Location extends Document {
    name : String
    country : String
}