import { RelationDto } from "../../relation/dto/relation.dto";
import { EventDto } from "../../event/dto/event.dto";
import ObjectID from 'mongoose';

export interface PersonDto {
    readonly _id : ObjectID [];
    readonly firstName : string;
    readonly lastName : string[];
    readonly birthdayDate : Date;
    readonly sexe : 'H'|'F';
    readonly relations : RelationDto [] | ObjectID [];
    readonly events : EventDto [] | ObjectID [];
}