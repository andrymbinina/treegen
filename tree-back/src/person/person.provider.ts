import { PersonSchema } from './schemas/person.schema';
import { PERSON_TOKEN, DATABASE_TOKEN } from '../constants';

export const PersonProvider = [{
    provide: PERSON_TOKEN,
    useFactory: (connection) => {
        return connection.model('person', PersonSchema);
    },
    inject: [DATABASE_TOKEN],
}]