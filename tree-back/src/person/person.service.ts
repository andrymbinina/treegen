import { Injectable, Inject } from '@nestjs/common';
import Model from 'mongoose'
import { PERSON_TOKEN } from '../constants';
import { Person } from './interfaces/person.interface';
import { PersonDto } from './dto/person.dto';
import { DatabaseService } from '../database/database.service';

@Injectable()
export class PersonService extends DatabaseService<PersonDto, Person> {
    constructor(@Inject(PERSON_TOKEN) private readonly personModel : Model<Person>){
        super(personModel)
    }
}
