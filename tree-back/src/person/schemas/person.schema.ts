import * as mongoose from 'mongoose'
import { Event } from '../../event/schemas/event.schema';
import { Relation } from '../../relation/schemas/relation.schema';

export const PersonSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : true
    },
    lastName : {
        type : [String]
    },
    birthdayDate : {
        type : Date,
        required : true
    },
    events : {
        type : [Event]
    },
    relations : {
        type : [Relation],
    },
    sexe : {
        type : String,
        enum : ['H', 'F'],
        required : true
    }
})