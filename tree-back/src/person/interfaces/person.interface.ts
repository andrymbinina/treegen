import { Event } from '../../event/interfaces/event.interface';
import { Relation } from '../../relation/interfaces/relation.interface';
import { Document, ObjectID } from 'mongoose'

export class Person extends Document{
    readonly firstName : string;
    readonly lastName : string[];
    readonly birthdayDate : Date;
    readonly sexe : 'H'|'F';
    readonly events : Event [] | ObjectID [];
    readonly relations : Relation [] | ObjectID [];
}