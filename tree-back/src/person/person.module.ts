import { Module } from '@nestjs/common';
import { PersonController } from './person.controller';
import { PersonProvider } from './person.provider';
import { PersonService } from './person.service';
import { DataBaseModule } from '../database/database.module';

@Module({
  imports : [DataBaseModule],
  providers: [...PersonProvider, PersonService],
  controllers: [PersonController]
})
export class PersonModule {}
