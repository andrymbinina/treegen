import { Controller, Get, Response, Put, Body } from '@nestjs/common';
import { PersonService } from './person.service';
import { Person } from './interfaces/person.interface';
import { PersonDto } from './dto/person.dto';

@Controller('person')
export class PersonController {
    constructor(private readonly personneService : PersonService){
    }

    @Get()
    async getAllPersonne():Promise<Person[]>{
        return this.personneService.getAll();
    }

    @Put()
    async savePersonne(@Body() personne : PersonDto):Promise<Person>{
        return this.personneService.insert(personne);
    }
}
