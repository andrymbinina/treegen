import { Location } from "../../location/interfaces/location.interface"

export type WeddingEvent = {
    date : Date
    location : Location []
}

export type BirthEvent = {
    date : Date
    location : Location
}