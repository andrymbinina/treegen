import * as mongoose from 'mongoose'
import { Location } from '../../location/schemas/location.schema';

const ObjectId = mongoose.Schema.Types.ObjectId;

export const BirthEvent = {
    person : {
        type : ObjectId,
        required : true
    },
    location : {
        type : {
            date : {
                type : Date,
                required : true
            },
            location : {
                type : Location
            }
        }
    }
}

export const WeddingEvent = {
    person : {
        type : ObjectId,
        required : true
    },
    location : {
        type : [
            {
                date : {
                    type : Date,
                    required : true
                },
                location : {
                    type : Location
                }
            }
        ]
    }
}

export const Event = {
    weddings : {
        type : [WeddingEvent]
    },
    births : {
        type : [BirthEvent]
    }
}

export const EventSchema = new mongoose.Schema(Event)