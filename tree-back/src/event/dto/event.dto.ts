import { WeddingEvent, BirthEvent } from "../types"
import ObjectID from 'mongoose'

export interface EventDto {
    readonly _id : ObjectID [];
    readonly weddings : WeddingEvent []
    readonly births : BirthEvent []
}