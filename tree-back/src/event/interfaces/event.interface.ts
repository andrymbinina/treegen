import { Document, ObjectID } from 'mongoose'
import { BirthEvent, WeddingEvent } from '../types';

export class Event extends Document {
    readonly weddings : WeddingEvent [] | ObjectID []
    readonly births : BirthEvent [] | ObjectID []
}